import java.util.Scanner;

public class Pot {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int total = 0;
        int cases = scan.nextInt();

        for (int i = 0; i < cases; i++) {
            String input = scan.next();
            int pow = Integer.parseInt(input.substring(input.length()-1));
            input = input.substring(0, input.length()-1);

            total += (int)Math.pow(Integer.parseInt(input),pow);
        }
        System.out.println(total);

    }
}
