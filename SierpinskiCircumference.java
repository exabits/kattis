import java.math.BigDecimal;
import java.util.Scanner;

public class SierpinskiCircumference {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        BigDecimal radius;
        int testCase,count = 1;

        while (scan.hasNextInt()) {
            testCase = scan.nextInt();

            radius = BigDecimal.valueOf(1.5);
            radius = radius.pow(testCase).multiply(BigDecimal.valueOf(3));

            System.out.println(radius.toBigInteger());
            long length = String.valueOf(radius.toBigInteger()).length();
            System.out.println("Case " + count + ": " + length);
            count++;
        }
    }
}