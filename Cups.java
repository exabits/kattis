import java.util.*;

public class Cups {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String input1, input2;

        int number = scan.nextInt();
        int radius = 0;
        LinkedHashMap<String, Integer> track = new LinkedHashMap<>();

        for (int i = 0; i < number; i++) {
            input1 = scan.next();
            input2 = scan.next();

            if (isNumeric(input1)) {
                radius = Integer.parseInt(input1) / 2;
                track.put(input2, radius);
            } else {
                radius = Integer.parseInt(input2);
                track.put(input1, radius);
            }
        }
        Map<String, Integer> track2 = sortByValue(track);

        for (String key : track2.keySet()) {
            System.out.println(key);
        }


        track2.forEach((key,value) -> {
            System.out.println(key);
        });

    }


    public static HashMap<String, Integer> sortByValue(HashMap<String, Integer> hm) {
        // Create a list from elements of HashMap
        List<Map.Entry<String, Integer>> list =
                new LinkedList<Map.Entry<String, Integer>>(hm.entrySet());

        // Sort the list
        Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {
            public int compare(Map.Entry<String, Integer> o1,
                               Map.Entry<String, Integer> o2) {
                return (o1.getValue()).compareTo(o2.getValue());
            }
        });

        // put data from sorted list to hashmap
        HashMap<String, Integer> temp = new LinkedHashMap<String, Integer>();
        for (Map.Entry<String, Integer> aa : list) {
            temp.put(aa.getKey(), aa.getValue());
        }
        return temp;
    }

    public static boolean isNumeric(String strNum) {
        try {
            double d = Double.parseDouble(strNum);
        } catch (NumberFormatException | NullPointerException nfe) {
            return false;
        }
        return true;
    }
}