#include <iostream>
#include <cmath>

using namespace std;

void points(){
    string str;
    int t = 0;
    int c = 0;
    int g = 0;
    int score = 0;

    cin >> str;
    if (str.size() <= 50){
        for (int i = 0; i < str.size(); i++){
            if (str.at(i) == 'T'){
                t++;
            }else if(str.at(i) == 'C') {
                c++;
            }else if(str.at(i) == 'G'){
                g++;
            }
        }
    }
    score = min(min(t, c), g) * 7;
    score += pow(t, 2) + pow(c, 2) + pow(g, 2);
    cout << score;
}

int main() {
    points();
    return 0;
}