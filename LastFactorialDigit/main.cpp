#include <iostream>

using namespace std;

void LastFactorial(){
    int num,sum;
    int factorial;
    cin >> num;

    while (num > 0){
        sum = 1;
        cin >> factorial;
        while (factorial > 0){
            sum *= factorial;

            factorial--;
        }

        cout << sum%10 << endl;
        num--;
    }
}

int main() {
    LastFactorial();
    return 0;
}