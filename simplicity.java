import java.util.*;

class simplicity {
    public static void main(String[] args) {
        Scanner scnr = new Scanner(System.in);

        String input = scnr.next();

        ArrayList<Character> arrInput = new ArrayList<>();
        ArrayList<Integer> test = new ArrayList<>();

        for (int i = 0; i < input.length(); i++) {
            arrInput.add(input.charAt(i));
        }

        Collections.sort(arrInput);

        int count = 0;
        for (int i = 0; i < arrInput.size(); i+=count) {
            count=0;
            for (int j = 0; j < arrInput.size(); j++) {
                if (arrInput.get(i) == arrInput.get(j))
                    count++;
            }
            test.add(count);
        }

        Collections.sort(test);
        Collections.reverse(test);

        count = 0;
        while (test.size() > 2){
            count += test.get(test.size()-1);
            test.remove(test.size()-1);
        }
        System.out.println(count);
    }
}
