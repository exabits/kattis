import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class HotHike {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        ArrayList<Integer> temps = new ArrayList<>();
        ArrayList<Integer> days = new ArrayList<>();

        int max;
        int tempCases = scan.nextInt();

        for (int i = 0; i < tempCases; i++) {
            int temp = scan.nextInt();
            temps.add(temp);
        }

        for (int i = 0; i < (temps.size() - 2); i++) {
            max = Math.max(temps.get(i), temps.get(i + 2));
            days.add(max);
        }

        ArrayList<Integer> it = new ArrayList<>(days);
        Collections.sort(it);

        for (int i = 0; i < it.size(); i++) {
            if (it.get(0).equals(days.get(i))) {
                System.out.print(days.indexOf(days.get(i)) + 1);
                System.out.println(" " + Math.max(temps.get(i), temps.get(i + 2)));
                break;
            }
        }
    }
}