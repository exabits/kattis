import java.util.Scanner;

public class Pianina {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int total = 2;
        int cases = scan.nextInt();

        for (int i = 0; i < cases; i++) {
            total += total - 1;  // 3, 5, 9, 17
        }
        System.out.println(total*total);
    }
}
