#include <bits/stdc++.h>

using namespace std;

int main() {
    string x, y;
    cin >> x >> y;
    vector<int> yoda;
    vector<int> yoda2;
    vector<int> answer;
    vector<int> answer2;

    for (int i = 0; i < x.size(); i++) {       //read strings into vectors
        yoda.push_back(x.at(i));
    }

    for (int i = 0; i < y.size(); i++) {
        yoda2.push_back(y.at(i));
    }

    while (yoda.size() != yoda2.size()) {   //fill lowest size vector with 0's
        if (yoda.size() < yoda2.size()) {
            yoda.insert(yoda.begin(), '0');
        }
        if (yoda.size() > yoda2.size()) {
            yoda2.insert(yoda2.begin(), '0');
        }
    }

    if (yoda.size() == yoda2.size()) {       //insert greatest digits to respective vector
        int i = 0;
        while (i < yoda.size()) {
            if (yoda.at(i) > yoda2.at(i)) {
                answer.push_back(yoda.at(i) - '0');
            } else if (yoda.at(i) < yoda2.at(i)) {
                answer2.push_back(yoda2.at(i) - '0');
            } else if (yoda.at(i) == yoda2.at(i)) {
                answer.push_back(yoda.at(i) - '0');
                answer2.push_back(yoda2.at(i) - '0');
            }
            i++;
        }
    }

    if (answer.empty()) {         //print out answer vectors
        cout << "YODA";
    } else if (answer.at(0) == 0 and answer.at(answer.size()-1) == 0) {
        cout << "0";
    } else {
        for (int i = 0; i < answer.size(); i++) {
            cout << answer[i];
        }
    }
    cout << endl;
    if (answer2.empty()) {         //print out answer2 vector
        cout << "YODA";
    } else if (answer2.at(0) == 0 and answer2.at(answer2.size()-1) == 0) {
        cout << "0";
    } else {
        for (int i = 0; i < answer2.size(); i++) {
            cout << answer2[i];
        }
    }
    return 0;
}
