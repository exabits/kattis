import java.util.ArrayList;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class IveBeenEverywhere {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int cases = scan.nextInt();
        ArrayList<String> data = new ArrayList<String>();

        for (int i = 0; i < cases; i++) {
            int tripsCount = scan.nextInt();
            for (int j = 0; j < tripsCount; j++) {
                data.add(scan.next());
            }
            Set<String> unique = new HashSet<>(data);
            System.out.println(unique.size());
            data.clear();
            unique.clear();
        }
    }
}
