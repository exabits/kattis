#include <bits/stdc++.h>

using namespace std;

int main() {
    string alpha[26] = {"@", "8", "(", "|)", "3", "#", "6", "[-]", "|", "_|", "|<", "1", "[]\\/[]", "[]\\[]", "0", "|D",
                        "(,)", "|Z", "$", "']['", "|_|", "\\/", "\\/\\/", "}{", "`/", "2"};
    string input;
    string output;

    getline(cin, input);

    for (int i = 0; i < input.size(); i++) {
        char c = tolower(input[i]);

        if (c >= 'a' && c <= 'z') {
            output.insert(output.size(), alpha[c - 'a']);
        } else {
            output.push_back(c);
        }
    }
    cout << output << endl;
    return 0;
}
