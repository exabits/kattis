import java.util.Scanner;

public class Tarifa {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int answer = 0;
        int megs = scan.nextInt();
        int loop = scan.nextInt();

        for (int i = 0; i < loop; i++) {
            int n = scan.nextInt();
            answer += megs - n;
        }
        answer += megs;
        System.out.println(answer);
    }
}
