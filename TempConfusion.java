import java.util.*;

public class TempConfusion {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        String A="",B="";
        String degrees = scan.next() ;
        int iend = degrees.indexOf("/");

        if (iend != -1)
        {
            A = degrees.substring(0 , iend);
            B = degrees.substring(iend+1);
        }

        int denomenator = (Integer.parseInt(B)*9);
        int numurator = (Integer.parseInt(A) - (32*Integer.parseInt(B))) * 5;

        if (numurator == 0 || Integer.parseInt(B) == 0)
            System.out.println(numurator + "/" + 1);
        else{
            System.out.println(numurator/Math.abs(gcm(numurator,denomenator)) + "/" + denomenator/Math.abs(gcm(numurator,denomenator)));
        }
    }
    public static int gcm(int a, int b) {
        return b == 0 ? a : gcm(b, a % b);
    }
}
