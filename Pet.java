import java.util.Scanner;

public class Pet {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int maxScore = 0;
        int winner = 0;

        for (int i = 1; i <= 5; i++) {
            int grade1 = scan.nextInt();
            int grade2 = scan.nextInt();
            int grade3 = scan.nextInt();
            int grade4 = scan.nextInt();

            int total = grade1+grade2+grade3+grade4;

            if (total > maxScore) {
                maxScore = total;
                winner = i;
            }
        }
        System.out.println(winner + " " + maxScore);
    }
}
