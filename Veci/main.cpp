
#include <algorithm>
#include <iostream>
using namespace std;
int main () {
    string s, t;
    cin >> s;
    t = s;
    next_permutation(t.begin(), t.end());
    cout << ( t >= s ? t : "0") << endl;
}