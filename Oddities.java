import java.util.Scanner;

public class Oddities {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int cases = scan.nextInt();

        for (int i = 0; i < cases; i++) {
            int test = scan.nextInt();

            if (Math.abs(test) % 2 == 1)
                System.out.println(test + " is odd");
            else
                System.out.println(test + " is even");
        }
    }
}
