import java.util.Scanner;

public class speedlimit {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int count = 0;
        int[] values = new int[100];

        do {
            int answer = 0;
            count = scan.nextInt();
            if (count == -1)
                break;

            for (int i = 0; i < count; i++) {
                int miles = scan.nextInt();
                values[i] = scan.nextInt();

                if(i == 0)
                    answer += miles * values[i];
                else
                    answer += miles * (values[i]-values[i-1]);
            }
            System.out.println(answer + " miles");
        }while (true);
    }
}
