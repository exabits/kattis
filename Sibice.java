import java.util.Scanner;

public class Sibice {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int matchCount = scan.nextInt();
        int width = scan.nextInt();
        int height = scan.nextInt();

        int hypot = (width*width)+(height*height);

        hypot = (int)Math.sqrt(hypot);

        for (int i = 0; i < matchCount; i++) {
            int matchSize = scan.nextInt();
            if (hypot >= matchSize)
                System.out.println("DA");
            else
                System.out.println("NE");

        }
    }
}
