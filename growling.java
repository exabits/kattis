import java.lang.reflect.Array;
import java.util.*;

class growling {
    public static void main(String[] args) {
        Scanner scnr = new Scanner(System.in);

        ArrayList<Double> gear = new ArrayList<>();

        int testCases = scnr.nextInt();
        int gears, a,b,c;
        double torque;

        for (int i = 0; i < testCases; i++) {
            gears = scnr.nextInt();

            for (int j = 1; j <= gears; j++) {
                double store = 0;
                a = scnr.nextInt();
                b = scnr.nextInt();
                c = scnr.nextInt();

                double deriv = (double)b/(2*a);
                torque = -a * Math.pow(deriv,2) + b * deriv + c;

                gear.add(torque);
                System.out.println(gear);
            }
            System.out.println(gear.indexOf(Collections.max(gear)) +1);

            gear.clear();
        }
    }
}
