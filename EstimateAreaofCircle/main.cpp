#include <bits/stdc++.h>

using namespace std;

int main() {
    float r,m,c,actuArea,estiArea;
    cin>>r>>m>>c;
    while(r!=0 && m!=0 && c!=0)
    {
        actuArea=M_PI*pow(r,2);
        estiArea=4*(c/m)*pow(r,2);
        cout<< setprecision(10) << actuArea <<" "<< estiArea << endl;
        cin>>r>>m>>c;
    }
    return 0;
}