import java.util.*;;

public class Palindrome {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String string = scan.nextLine();

        isPalindromeUsingStringBuilder(string);

        if (isPalindromeUsingStringBuilder(string)){
            System.out.println(string + " is a palindrome");
        }else{
            System.out.println(string + " is not a palindrome");
        }
    }

    private static boolean isPalindromeUsingStringBuilder(String text) {
        String clean = text.replaceAll("\\s+", "").toLowerCase();
        StringBuilder plain = new StringBuilder(clean);
        StringBuilder reverse = plain.reverse();
        return (reverse.toString()).equals(clean);
    }
}
