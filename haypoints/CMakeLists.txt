cmake_minimum_required(VERSION 3.14)
project(haypoints)

set(CMAKE_CXX_STANDARD 17)

add_executable(haypoints main.cpp)