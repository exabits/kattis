#include <bits/stdc++.h>
using namespace std;

int main() {
    int N, M, sum;
    cin >> M >> N;

    map<string, int> it;

    for (int i = 0; i < M; i++) {
        string word;
        int value;
        cin >> word >> value;
        it[word] = value;
    }

    for (int i = 0; i < N; i++) {
        sum = 0;
        while (true) {
            string word;
            cin >> word;
            if (word == ".") {
                break;
            } else {
                if (it.count(word)) {
                    sum += it[word];
                }
            }
        }
        cout << endl << sum;
    }
    return 0;
}