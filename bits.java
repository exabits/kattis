import java.util.*;

public class bits {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int cases = scan.nextInt();

        for (int i = 0; i < cases; i++) {
            String num = scan.next();
            int count = 0;

            for (int j = 1; j <= num.length(); j++) {
                int bitCountperNew = Long.bitCount(Long.parseLong(num.substring(0 , j)));

                if (bitCountperNew > count){
                    count = bitCountperNew;
                }
            }
            System.out.println(count);
        }
    }
}
