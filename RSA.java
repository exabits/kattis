import java.util.*;

class RSA {
    public static void main(String[] args) {
        long startTime = System.nanoTime();
        final long start = System.currentTimeMillis();
        Scanner scan = new Scanner(System.in);
        long sample = scan.nextLong();

        for (int i = 0; i < sample; i++) {
            long N = scan.nextLong();
            long e = scan.nextLong();

            long r = (primeFactors(N).get(0) - 1) * (primeFactors(N).get(1) - 1);
            System.out.println(modInverse(e, r));

            long endTime = System.nanoTime();
            long totalTime = endTime - startTime;
            double elapsedTimeInSecond = (double) totalTime / 1_000_000_000;
            System.out.println(elapsedTimeInSecond + " seconds");
        }
    }

    static List<Long> primeFactors(long number) {
        long n = number;
        List<Long> primefactors = new ArrayList<>();
        for (long i = 2; i <= n; i++) {
            while (n % i == 0) {
                primefactors.add(i);
                n /= i;
            }
        }
        return primefactors;
    }

    static long modInverse(long e, long r) {
        e = e % r;
        int count = 0;
        for (long x = r + 1; x < (r * 1000000) + 1; x += r) {
            System.out.println(count + "   " + x);
            if (x % e == 0) {
                return x / e;
            }
            count++;
        }
        return -1;
    }
}
