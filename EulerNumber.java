import java.util.*;

public class EulerNumber {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int number;
        double fraction = 0;
        number = scan.nextInt();

        for (int i = 0; i <= number; i++) {
            fraction += (double)1 / factorial(i);
        }

        System.out.println(fraction);

    }

    static double factorial(int n) {
        if (n == 0)
            return 1;
        else
            return (n * factorial(n - 1));
    }
}
