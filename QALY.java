import java.util.Scanner;

public class QALY {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        double total = 0;
        int cases = scan.nextInt();

        for (int i = 0; i < cases; i++) {
            double ratio = scan.nextDouble();
            double point = scan.nextDouble();

            total += ratio * point;
        }
        System.out.printf("%.3f",total);
    }
}